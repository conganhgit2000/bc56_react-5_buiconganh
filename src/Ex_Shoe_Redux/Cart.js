import React, { Component } from "react";
import { connect } from "react-redux";
import { quantityShoeAction, removeShoeAction } from "./redux/action/shoe";

class Cart extends Component {
  renderTbody = () => {
    let { cart, handleRemove, handleChangeQuantity } = this.props;
    return cart.map((item) => {
      return (
        <tr>
          <td>${item.name}</td>
          <td>${item.price}</td>
          <td>
            <img width={100} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong>{item.soLuong}</strong>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, +1);
              }}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (id) => {
      dispatch(removeShoeAction(id));
    },
    handleChangeQuantity: (id, option) => {
      dispatch(quantityShoeAction(id, option));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
