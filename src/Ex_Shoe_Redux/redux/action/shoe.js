import {
  BUY_SHOE,
  QUANTITY_SHOE,
  REMOVE_SHOE,
  VIEW_DETAIL,
} from "../constant/shoe";

export let buyShoeAction = (shoe) => {
  return {
    type: BUY_SHOE,
    payload: shoe,
  };
};
export let viewShoeAction = (shoe) => {
  return {
    type: VIEW_DETAIL,
    payload: shoe,
  };
};
export let removeShoeAction = (id) => {
  return {
    type: REMOVE_SHOE,
    payload: id,
  };
};
export let quantityShoeAction = (id, option) => {
  return {
    type: QUANTITY_SHOE,
    payload: {
      id: id,
      option: option,
    },
  };
};
