import { message } from "antd";
import { shoeArr } from "../../data";
import {
  BUY_SHOE,
  QUANTITY_SHOE,
  REMOVE_SHOE,
  VIEW_DETAIL,
} from "../constant/shoe";

let initialState = {
  shoeArr: shoeArr,
  detail: shoeArr[1],
  cart: [],
};

export let shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_SHOE: {
      let cloneCart = [...state.cart];
      var index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        //không tìm thấy ==> push
        let newShoe = { ...action.payload, soLuong: 1 };
        cloneCart.push(newShoe);
      } else {
        //tìm thấy không push thêm chỉ update số lượng hiện tại của phần tử được chọn trong mảng
        cloneCart[index].soLuong++;
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case VIEW_DETAIL: {
      return { ...state, detail: action.payload };
    }
    case REMOVE_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === action.payload);
      cloneCart.splice(index, 1);
      message.success("xóa thành công");
      return { ...state, cart: cloneCart };
    }
    case QUANTITY_SHOE: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === action.payload.id);
      cloneCart[index].soLuong =
        cloneCart[index].soLuong + action.payload.option;
      //nếu số lượng sau khi thay đổi bằng 0 thì tiến hành xóa\
      cloneCart[index].soLuong === 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
